const path = require('path');

const { UserScriptMetaDataPlugin } = require('userscript-metadata-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const { merge } = require('webpack-merge');

const metadata = require('./metadata.cjs');
const webpackConfig = require('./webpack.common.cjs');

metadata.require.push(
  'file://' + path.resolve(__dirname, '../dist/stores-to-agents.debug.user.js')
);

const cfg = merge(webpackConfig, {
  entry: {
    debug: webpackConfig.entry,
    dev: path.resolve(__dirname, './empty.cjs'),
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'stores-to-agents.[name].user.js'
  },
  devtool: 'eval-source-map',
  watch: true,
  watchOptions: {
    ignored: /node_modules/,
  },
  plugins: [
    new LiveReloadPlugin({ delay: 500 }),
    new UserScriptMetaDataPlugin({ metadata }),
  ],
});

module.exports = cfg;
