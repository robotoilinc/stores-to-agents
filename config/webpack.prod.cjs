const { UserScriptMetaDataPlugin } = require('userscript-metadata-webpack-plugin');
const { merge } = require('webpack-merge');

const metadata = require('./metadata.cjs');
const webpackConfig = require('./webpack.common.cjs');
const path = require('path');

const cfg = merge(webpackConfig, {
  mode: 'production',
  output: {
    path: path.resolve(__dirname, '../release'),
    filename: 'stores-to-agents.user.js'
  },
  optimization: {
    chunkIds: 'named',
    concatenateModules: true,
    flagIncludedChunks: true,
    emitOnErrors: true,
    innerGraph: true,
    mangleExports: false,
    mangleWasmImports: false,
    mergeDuplicateChunks: true,
    minimize: false,
    moduleIds: 'named',
    providedExports: true,
    realContentHash: true,
    removeAvailableModules: true,
    removeEmptyChunks: true,
    usedExports: true,
  },
  plugins: [
    new UserScriptMetaDataPlugin({ metadata }),
  ],
});

module.exports = cfg;
