import GMStorage from "gm-storage";
import Logger from "js-logger";

import {LoginInterface} from "./Logins";
import {PandaBuyToken, PandaBuyUserInfo} from "../../Constants";
import {PandaBuyError} from "../../exceptions/PandaBuyError";
import {LocalStorage} from "../../helpers/StorageHelper";

export class PandaBuyLogin implements LoginInterface {
  private store: GMStorage;
  private localStorage: LocalStorage;

  constructor() {
    this.store = new GMStorage()
    this.localStorage = new LocalStorage();
  }

  public supports(hostname: string): boolean {
    return hostname.includes("pandabuy.com");
  }

  public process(): void {
    // If we already have a token, don't bother
    const currentToken = this.store.get(PandaBuyToken, null);
    if (currentToken !== null && (currentToken as string).length !== 0) {
      return;
    }

    // Don't bother with getting the token, if we aren't loggeed in yet
    const userInfo = this.localStorage.get(PandaBuyUserInfo);
    if (userInfo === null || userInfo.length === 0) {
      return;
    }

    // The token should now exist
    const updatedToken = this.localStorage.get(PandaBuyToken);
    if (updatedToken === null || updatedToken.length === 0) {
      throw new PandaBuyError("Could not retrieve token");
    }

    // Store it internally
    this.store.set(PandaBuyToken, updatedToken);
    Logger.info("Updated the PandaBuy Authorization Token");
  }
}
