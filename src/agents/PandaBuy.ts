import GMStorage from "gm-storage";
import Logger from "js-logger";

import {AgentInterface} from "./Agents";
import {PandaBuyToken} from "../Constants";
import {Order} from "../classes/Order";
import {PandaBuyError} from "../exceptions/PandaBuyError";
import {determineStoreSource} from "../helpers/DetermineStoreSource";
import {post} from "../helpers/Fetch";

export class PandaBuy implements AgentInterface {
  private store: GMStorage;

  constructor() {
    this.store = new GMStorage()
  }

  get name(): string {
    return "PandaBuy";
  }

  async send(order: Order): Promise<void> {
    const token = this.store.get(PandaBuyToken, null);
    if (token === null || (token as string).length === 0) {
      throw new PandaBuyError("We do not have your PandaBuy authorization token yet. Please visit PandaBuy (and login if needed)");
    }

    // Build the purchase data
    const purchaseData = this._buildPurchaseData(order);

    Logger.info("Sending order to PandaBuy...", purchaseData);

    // Do the actual call
    await post("https://www.pandabuy.com/gateway/user/cart/add", {
      credentials: "include",
      mode: "cors",
      referrer: `https://www.pandabuy.com/uniorder?text=${encodeURIComponent(window.location.href)}`,
      referrerPolicy: "strict-origin-when-cross-origin",
      body: JSON.stringify(purchaseData),
      headers: {
        "accept": "application/json, text/plain, */*",
        "accept-language": "nl,en-US;q=0.9,en;q=0.8,de;q=0.7,und;q=0.6",
        "authorization": `Bearer ${token}`,
        "content-type": "application/json;charset=UTF-8",
        "currency": "CNY",
        "device": "pc",
        "lang": "en",
      },
    }).then(async (response) => {
      const data = await response.json();
      if (response.status === 200 && data.msg === null) {
        return;
      }

      // Our token has expired
      if (response.status === 401) {
        // Reset the current token
        GM_setValue(PandaBuyToken, null);

        Logger.error("PandaBuy authorization token has expired");
        throw new PandaBuyError("Your PandaBuy authorization token has expired. Please visit PandaBuy (or login at PandaBuy) again");
      }

      Logger.error("Item could not be added", data.msg);
      throw new PandaBuyError("Item could not be added");
    }).catch((err) => {
      // If the error is our own, just rethrow it
      if (err instanceof PandaBuyError) {
        throw err;
      }

      Logger.error("An error happened when uploading the order", err);
      throw new Error("An error happened when adding the order");
    });
  }

  private _buildPurchaseData(order: Order): object {
    // Build the description
    const description = this._buildRemark(order);

    // Create the purchasing data
    return {
      storeName: order.shop.name,
      storeId: order.shop.id,
      goodsUrl: window.location.href,
      goodsName: order.item.name,
      goodsNameCn: order.item.name,
      goodsAttr: description,
      goodsAttrCn: description,
      storageNo: 1,
      goodsPrice: order.price,
      goodsNum: 1,
      fare: order.shipping,
      remark: "",
      selected: 1,
      purchaseType: 1,
      goodsImg: order.item.imageUrl,
      servicePrice: "0.00",
      writePrice: order.price,
      actPrice: order.price,
      storeSource: determineStoreSource(),
      goodsId: order.item.id,
      seller: order.shop.name,
      storeUrl: "https://weidian.com/?userid=" + order.shop.id,
    };
  }

  private _buildRemark(order: Order): string | null {
    const descriptionParts: string[] = [];
    if (order.item.model !== null && order.item.model.length !== 0) descriptionParts.push(`Model: ${order.item.model}`);
    if (order.item.color !== null && order.item.color.length !== 0) descriptionParts.push(`Color: ${order.item.color}`);
    if (order.item.size !== null && order.item.size.length !== 0) descriptionParts.push(`Size: ${order.item.size}`);
    if (order.item.other.length !== 0) descriptionParts.push(`${order.item.other}`);

    let description = null;
    if (descriptionParts.length !== 0) {
      description = descriptionParts.join(" / ");
    }

    return description;
  }
}
