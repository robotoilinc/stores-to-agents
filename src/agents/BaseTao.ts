import Logger from "js-logger";
import {serialize} from "object-to-formdata";

import {AgentInterface} from "./Agents";
import {Order} from "../classes/Order";
import {BaseTaoError} from "../exceptions/BaseTaoError";
import {determineStoreSource} from "../helpers/DetermineStoreSource";
import {get, post} from "../helpers/Fetch";
import {removeEmoji} from "../helpers/RemoveEmoji";

const CSRF_REQUIRED_ERROR = "You need to be logged in on BaseTao to use this extension (CSRF required).";

export class BaseTao implements AgentInterface {
  private parser: DOMParser;

  constructor() {
    this.parser = new DOMParser();
  }

  get name(): string {
    return "BaseTao";
  }

  async send(order: Order): Promise<void> {
    // Get proper domain to use
    const properDomain = await this._getDomain();

    // Build the purchase data
    const purchaseData = await this._buildPurchaseData(properDomain, order);

    Logger.info("Sending order to BaseTao...", properDomain, order);

    // Do the actual call
    const response = await post(`${properDomain}/best-taobao-agent-service/bt_action/add_cart`, {
      body: new URLSearchParams(serialize(purchaseData) as any),
      referrer: `${properDomain}/best-taobao-agent-service/how_make/buy_order.html`,
      headers: {
        origin: `${properDomain}`,
        referrer: `${properDomain}/best-taobao-agent-service/how_make/buy_order.html`,
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Safari/537.36",
        "x-requested-with": "XMLHttpRequest",
      },
    });

    const responseData = await response.json();
    if (responseData.value === "1") {
      return;
    }

    Logger.error("Item could not be added", response);
    throw new BaseTaoError("Item could not be added, make sure you are logged in");
  }

  private async _getDomain(): Promise<string> {
    // Try HTTPS (with WWW) first
    let response = await get("https://www.basetao.com/best-taobao-agent-service/how_make/buy_order.html");
    let doc = this.parser.parseFromString(await response.text(), "text/html");
    let csrfToken = doc.querySelector<HTMLInputElement>("input[name=bt_sb_token]");
    if (csrfToken && csrfToken.value.length !== 0) {
      return "https://www.basetao.com";
    }

    // Try HTTPS (without WWW) after
    response = await get("https://basetao.com/best-taobao-agent-service/how_make/buy_order.html");
    doc = this.parser.parseFromString(await response.text(), "text/html");
    csrfToken = doc.querySelector<HTMLInputElement>("input[name=bt_sb_token]");
    console.log(csrfToken);
    if (csrfToken && csrfToken.value.length !== 0) {
      return "https://basetao.com";
    }

    // User is not logged in/there is an issue
    throw new Error(CSRF_REQUIRED_ERROR);
  }

  private async _buildPurchaseData(properDomain: string, order: Order): Promise<object> {
    // Get the CSRF token
    const csrf = await this._getCSRF(properDomain);

    // Build the data we will send
    const data = {
      addtime: Date.now(),
      goodscolor: order.item.color ?? "-",
      goodsimg: order.item.imageUrl,
      goodsname: removeEmoji(order.item.name),
      goodsnum: 1,
      goodsprice: order.price,
      goodsremark: this._buildRemark(order) ?? "",
      goodsseller: removeEmoji(order.shop.name ?? ""),
      goodssite: determineStoreSource(),
      goodssize: order.item.size ?? "-",
      goodsurl: window.location.href,
      item_id: order.item.id,
      sellerurl: order.shop.url ?? "",
      sendprice: order.shipping,
      siteurl: window.location.hostname,
      sku_id: 0,
      type: 1,
    };

    return {
      bt_sb_token: csrf,
      data: JSON.stringify(data)
    };
  }

  private async _getCSRF(properDomain: string): Promise<string> {
    // Grab data from BaseTao
    const response = await get(`${properDomain}/best-taobao-agent-service/how_make/buy_order.html`);

    // Check if user is actually logged in
    const data = await response.text();
    if (data.includes("please sign in again")) {
      throw new Error(CSRF_REQUIRED_ERROR);
    }

    const doc = this.parser.parseFromString(data, "text/html");
      const csrfToken = doc.querySelector<HTMLInputElement>("input[name=bt_sb_token]");
    if (csrfToken && csrfToken.value.length !== 0) {
      return csrfToken.value;
    }

    // Return CSRF
    throw new Error(CSRF_REQUIRED_ERROR);
  }

  private _buildRemark(order: Order): string | null {
    const descriptionParts: (string | any)[] = [];
    if (order.item.model !== null) descriptionParts.push(`Model: ${order.item.model}`);
    if (order.item.other.length !== 0) descriptionParts.push(order.item.other);

    let description = null;
    if (descriptionParts.length !== 0) {
      description = descriptionParts.join(" / ");
    }

    return description;
  }
}
