export class Enum {
  private readonly _model: string[] = ["型号", "模型", "模型", "model", "type"];
  private readonly _colors: string[] = ["颜色", "彩色", "色", "色彩", "配色", "配色方案", "color", "colour", "color scheme"];
  private readonly _sizing: string[] = ["尺寸", "尺码", "型号尺寸", "大小", "浆液", "码数", "码", "size", "sizing"];

  public isModel(item: string): boolean {
    return this._arrayContains(this._model, item);
  }

  public isColor(item: string): boolean {
    return this._arrayContains(this._colors, item);
  }

  public isSize(item: string): boolean {
    return this._arrayContains(this._sizing, item);
  }

  private _arrayContains(array: string[], query: string): boolean {
    return array.filter((item: string) => query.toLowerCase().indexOf(item.toLowerCase()) !== -1).length !== 0;
  }
}
