import {capitalize} from "./Capitalize";
import {removeWhitespaces} from "./RemoveWhitespaces";
import {Enum} from "../Enums";

export const retrieveDynamicInformation = ($document: JQuery<Document>, rowCss: string, rowTitleCss: string, selectedItemCss: string): { model: string | null; color: string | null; size: string | null; others: string[] } => {
  // Create dynamic items
  let model = null;
  let color = null;
  let size = null;
  const others: string[] = [];

  // Load dynamic items
  $document.find(rowCss).each((key, value) => {
    const _enum = new Enum();
    const rowTitle = $(value).find(rowTitleCss).text();
    const selectedItem = $(value).find(selectedItemCss);

    // Check if this is model
    if (_enum.isModel(rowTitle)) {
      if (selectedItem.length === 0) {
        throw new Error("Model is missing");
      }

      model = removeWhitespaces(selectedItem.text());
      return;
    }

    // Check if this is color
    if (_enum.isColor(rowTitle)) {
      if (selectedItem.length === 0) {
        throw new Error("Color is missing");
      }

      color = removeWhitespaces(selectedItem.text());
      return;
    }

    // Check if this is size
    if (_enum.isSize(rowTitle)) {
      if (selectedItem.length === 0) {
        throw new Error("Sizing is missing");
      }

      size = removeWhitespaces(selectedItem.text());
      return;
    }

    others.push(`${capitalize(rowTitle)}: ${removeWhitespaces(selectedItem.text())}`);
  });

  return {model, color, size, others};
};
