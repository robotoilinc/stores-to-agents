/**
 * @param s {string|undefined}
 * @returns {string}
 */
export const capitalize = (s: string | any[]): string => (s && s[0].toUpperCase() + s.slice(1)) || "";
