import GM_fetch from "@trim21/gm-fetch";

export function get(url: string, init?: RequestInit): Promise<Response> {
  return GM_fetch(url, {...init, method: "GET"})
}

export function post(url: string, request: RequestInit): Promise<Response> {
  return GM_fetch(url, {...request, method: "POST"})
}
