import {Item} from "./Item";
import {Shop} from "./Shop";

export class Order {
  public readonly shop: Shop;
  public readonly item: Item;
  public readonly price: number;
  public readonly shipping: number;

  constructor(shop: Shop, item: Item, price: number, shipping: number) {
    this.shop = shop;
    this.item = item;
    this.price = price;
    this.shipping = shipping;
  }
}
