import Logger from "js-logger";

import getLogin from "./agents/login/Logins";
import getStore from "./stores/Stores";

// Inject config styling
GM_addStyle("div.config-dialog.config-dialog-ani { z-index: 2147483647; }");

// Setup proper settings menu
GM_config.init("Settings", {
  agentSection: {
    label: "Select your agent",
    type: "section",
  },
  agentSelection: {
    label: "Your agent",
    type: "select",
    default: "empty",
    options: {
      empty: "Select your agent...",
      basetao: "BaseTao",
      cssbuy: "CSSBuy",
      pandabuy: "PandaBuy",
      superbuy: "SuperBuy",
      wegobuy: "WeGoBuy",
    },
  },
});

// Reload page if config changed
GM_config.onclose = (saveFlag: boolean) => { if (saveFlag) { window.location.reload(); } };

// Register menu within GM
GM_registerMenuCommand("Settings", GM_config.open);

// eslint-disable-next-line func-names
(async function () {
  // Setup the logger.
  Logger.useDefaults();

  // Log the start of the script.
  Logger.info(`Starting extension '${GM_info.script.name}', version ${GM_info.script.version}`);

  // Check if we are on a store
  const store = getStore(window.location.hostname);
  if (store !== null) {
    // If we have a store handler, attach and start
    store.attach($(window.document), window.unsafeWindow);

    return;
  }

  // Check if we are on a reshipping agent
  const login = getLogin(window.location.hostname);
  if (login !== null) {
    // If we are on one, execute whatever needed for that
    login.process();

    return;
  }

  Logger.error("Unsupported website");
}());
