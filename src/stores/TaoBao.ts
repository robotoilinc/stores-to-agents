import {Store} from "./Stores";
import {getAgent} from "../agents/Agents";
import {Item} from "../classes/Item";
import {Order} from "../classes/Order";
import {Shop} from "../classes/Shop";
import {elementReady} from "../helpers/ElementReady";
import {removeWhitespaces} from "../helpers/RemoveWhitespaces";
import {retrieveDynamicInformation} from "../helpers/RetrieveDynamicInformation";
import {Snackbar} from "../helpers/Snackbar";

export class TaoBao implements Store {
  public attach($document: JQuery<Document>, localWindow: Window): void {
    // If not logged in, show a snackbar
    elementReady("[class^=SecurityContent--rightTips]").then(() => {
      Snackbar("Please login before you use this script");
    });

    // If logged in, just continue
    elementReady("[class^=Actions--root]").then((element) => {
      const button = this._buildButton($document, localWindow);
      if (button === null) {
        return;
      }

      $(element).after(button)
    });
  }

  public supports(hostname: string): boolean {
    return hostname.includes("taobao.com");
  }

  private _buildButton($document: JQuery<Document>, window: Window): JQuery|null {
    // Force someone to select an agent
    if (GM_config.get("agentSelection") === "empty") {
      GM_config.open();

      Snackbar("Please select what agent you use");
      return null;
    }

    // Get the agent related to our config
    const agent = getAgent(GM_config.get("agentSelection"));

    const $button = $(`<button id="agent-button">Add to ${agent.name}</button>`)
      .css("width", "288px")
      .css("color", "#FFF")
      .css("border-color", "#F40")
      .css("background", "#F40")
      .css("cursor", "pointer")
      .css("text-align", "center")
      .css("font-family", '"Hiragino Sans GB","microsoft yahei",sans-serif')
      .css("font-size", "16px")
      .css("line-height", "38px")
      .css("border-width", "1px")
      .css("border-style", "solid")
      .css("border-radius", "2px");

    $button.on("click", async () => {
      // Disable button to prevent double clicks and show clear message
      $button.attr("disabled", "disabled").text("Processing...");

      // Try to build and send the order
      try {
        await agent.send(this._buildOrder($document, window));
      } catch (err: any) {
        $button.attr("disabled", null).text(`Add to ${agent.name}`);
        return Snackbar(err);
      }

      $button.attr("disabled", null).text(`Add to ${agent.name}`);

      // Success, tell the user
      return Snackbar("Item has been added, be sure to double check it");
    });

    return $('<div class="tb-btn-add-agent" style="margin-top: 20px"></div>').append($button);
  }

  private _buildShop($document: JQuery<Document>,window: Window | any): Shop {
    const storeURL = $document.find("a[class^=ShopHeader]").first().attr("href") as string;
    const authorMatches = storeURL.match(/\/\/(.+)\.taobao\.com/);

    const id = (authorMatches ? authorMatches[1] : storeURL);
    const name = $document.find("[class*=-shopName-]").text();
    const url = new URL(storeURL, window.location).toString();

    return new Shop(id, name, url);
  }

  private  _buildItem($document: JQuery<Document>, window: Window | any): Item {
    // Build item information
    const id = new URLSearchParams(window.location.search).get("id") as string;
    const name = $document.find("h1[class^=ItemTitle--mainTitle]").text();

    // Build image information
    const picSrc = $document.find("img[class^=PicGallery--mainPic]").first().attr("src") as string;
    const imageUrl = new URL(picSrc, window.location).toString();

    // Retrieve the dynamic selected item
    const { model, color, size, others } = retrieveDynamicInformation($document, "[class^=SkuContent--skuItem]", "[class^=ItemLabel--labelText]", "[class*=SkuContent--isSelected]");

    return new Item(id, name, imageUrl, model, color, size, others);
  }

  private  _buildPrice($document: JQuery<Document>): number {
    return Number(removeWhitespaces($document.find("[class^=Price--priceText]").text()));
  }

  private _buildShipping($document: JQuery<Document>): number {
    const postageText = removeWhitespaces($document.find("#J_WlServiceInfo").first().text());

    // Check for free shipping
    if (postageText.includes("快递 免运费")) {
      return 0;
    }

    // Try and get postage from text
    const postageMatches = postageText.match(/([\d.]+)/);

    // If we can't find any numbers, assume free as well, agents will fix it
    return postageMatches !== null ? Number(postageMatches[0]) : 0;
  }

  private _buildOrder($document: JQuery<Document>, window: Window): Order {
    return new Order(this._buildShop($document, window), this._buildItem($document, window), this._buildPrice($document), this._buildShipping($document));
  }
}
