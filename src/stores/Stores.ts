import {Store1688} from "./1688";
import {TaoBao} from "./TaoBao";
import {Tmall} from "./Tmall";
import {Weidian} from "./Weidian";
import {Yupoo} from "./Yupoo";

export interface Store {
  attach($document: JQuery<Document>, localWindow: Window): void;

  supports(hostname: string): boolean;
}

export default function getStore(hostname: string): Store|null {
  const agents = [new Store1688(), new TaoBao(), new Tmall(), new Yupoo(), new Weidian()];

  let agent = null;
  Object.values(agents).forEach((value) => {
    if (value.supports(hostname)) {
      agent = value;
    }
  });

  return agent;
}
