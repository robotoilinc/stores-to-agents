# Stores to Agents

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/B0B7CTDVS)

This simple userscript allows you to send 1688/Taobao/Tmall/Weidian/Yupoo orders directly into an agent.

## Supported agents:
- BaseTao 🔒
- CSSBuy
- PandaBuy 🔒
- Superbuy
- WeGoBuy

🔒 = _You need to be logged in_

---

## 1688

As of right now, 1688 **only** works on the newer React-based pages like [this one](https://detail.1688.com/offer/585002731161.html). Because of how 1688 is structured, [you can just buy multiple models/colors/etc of the same thing at once](https://i.imgur.com/uwawpkw.png), so all your selections will be [added to the order notes](https://i.imgur.com/dBgDvhG.png).

---
